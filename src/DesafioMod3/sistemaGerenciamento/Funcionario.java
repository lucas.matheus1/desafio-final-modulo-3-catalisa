package DesafioMod3.sistemaGerenciamento;

import DesafioMod3.dominio.IO;

public class Funcionario {

    //Metódo onde eu passo 3 consultores meus funcionarios...
    public static void faleComUmVendedor() {

        IO.mostrarComentario("Fale com uns dos nossos consultores: ");

        String[] listaDeConsutores = {"Lucas Matheus", "André Vicente", "Maga Alonso"};


        listaDeConsutores[0] = "Jose";
        listaDeConsutores[1] = "André";
        listaDeConsutores[2] = "Maga";


        for (int i = 0; i < listaDeConsutores.length; i++) {
            System.out.println(listaDeConsutores[i]);

        }

        System.out.println("----------------------------------------");
        IO.mostrarComentario("Digite seus dados para obter mais informações sobre os imoveis: ");


        String nome, telefone, email;
        int cont = 1;

        System.out.println("Digite seu nome: ");
        nome = IO.usuarioDigita().nextLine();

        System.out.println("Digite seu telefone: ");
        telefone = IO.usuarioDigita().nextLine();

        IO.mostrarComentario("Digite seu e-mail: ");
        email = IO.usuarioDigita().nextLine();


        while (cont != 2) {

            System.out.println(" ");
            System.out.println("Obter mais informações: ");
            System.out.println("Digite 1 para enviar: ");
            System.out.println("Digite 2 para sair: ");

            int fale = IO.usuarioDigita().nextInt();

            if (fale == 1) {
                System.out.println("Entraremos em contato! " + nome + " :)");
                break;

            } else {
                System.out.println("Volte sempre! ");
                break;
            }
        }


    }
}
