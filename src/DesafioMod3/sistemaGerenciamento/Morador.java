package DesafioMod3.sistemaGerenciamento;

import DesafioMod3.dominio.Pessoa;

import java.util.ArrayList;
import java.util.List;

public class Morador {


    //Atributo onde o Corpo e a classe pessoa;
    private Pessoa morador;

    private static List<Morador> validarMoradores = new ArrayList<>();

    //Metódo Construtor...
    public Morador(Pessoa morador) {
        this.morador = morador;
    }

    //Metódo seletores e modificadores...
    public Pessoa getMorador() {
        return morador;
    }

    public void setMorador(Pessoa morador) {
        this.morador = morador;
    }


    @Override
    public String toString() {
        System.out.println("Morador(a)");
        System.out.println(morador);
        return " ";
    }
}
