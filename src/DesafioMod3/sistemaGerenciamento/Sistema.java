package DesafioMod3.sistemaGerenciamento;

import DesafioMod3.dominio.IO;
import DesafioMod3.dominio.Imovel;
import DesafioMod3.dominio.Pessoa;
import DesafioMod3.sistemaGerenciamento.Funcionario;
import DesafioMod3.sistemaGerenciamento.Imobiliaria;
import DesafioMod3.sistemaGerenciamento.Morador;

public class Sistema {

    //Atributo da classe imobiliaria onde eu já instancio e já passo o nome do imobiliaria...
    private Imobiliaria imobiliaria = new Imobiliaria("Vilag");

    //Metódo que mostra as opçõpes para o usuário.
    public void menu(){

        System.out.println("Digite 1 para cadastrar um imovel: ");
        System.out.println("Digite 2 para exibir a lista de imoveis");
        System.out.println("Digite 3 para falar com um consultor de vendas: ");
        System.out.println("Digite 4 para alugar ou comprar o imovel: ");
        System.out.println("Digite 5 para exibir o morador do imovel: ");
        System.out.println("Digite 6 para sair");
        System.out.println("Digite 7 para excluir morador: ");
        System.out.println("------------------------");

    }

    //Metódo que cadastra um morador em um imovél.
    public void cadastroDeMorador(){

        String nome, telefone, cpf, endereco;

        System.out.println("Adicione um morador: ");


        IO.mostrarComentario("Digite seu nome: ");
        nome = IO.usuarioDigita().nextLine();

        IO.mostrarComentario("Digite seu telefone: ");
        telefone = IO.usuarioDigita().nextLine();

        IO.mostrarComentario("Digite seu CPF: ");
        cpf = IO.usuarioDigita().nextLine();

        IO.mostrarComentario("Digite seu endereco: ");
        endereco = IO.usuarioDigita().nextLine();

        Morador morador = new Morador(new Pessoa(nome, telefone, cpf));

        Imovel imovel = null;

        try {
            imovel = imobiliaria.encontrarImovel(endereco);
            imovel.adicionarMoradorNoImovel(morador);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //Metódo para cadastrar um Imovel...
    public void cadastrarImovel(){

        System.out.println("Adicione um Imovel: ");

        String nomeImobiliaria, responsavel, enderecoImovel;
        double valorDoImovel, valorDoAluguel;


        IO.mostrarComentario("Valor do Imovel: ");
        valorDoImovel = IO.usuarioDigita().nextDouble();

        IO.mostrarComentario("Digite o endereço do imovel: ");
        enderecoImovel = IO.usuarioDigita().nextLine();

        IO.mostrarComentario("Valor do Aluguel Imovel: ");
        valorDoAluguel = IO.usuarioDigita().nextDouble();


        try {
            var imovelEncontrado = imobiliaria.encontrarImovel(enderecoImovel);
            System.out.println("O imovel já está cadastrado.");

        } catch (Exception e) {
            Imovel imovel = new Imovel(valorDoImovel, enderecoImovel, valorDoAluguel);

            imobiliaria.adicionarImovel(imovel);

        }

    }


    public void mostraImovel(){

        imobiliaria.mostrarListaDeImovel();

    }

    //Metódo para mostrar o morador, onde para eu mostrar preciso do endereço do imovel...
    public void mostrarMorador(){

        String endereco; // Variavel endereco

        IO.mostrarComentario("Digite seu endereço: ");
        endereco = IO.usuarioDigita().nextLine();

        Imovel imovel = null;

        //Fazendo o tratamento com Try
        try {
            imovel = imobiliaria.encontrarImovel(endereco);
            imovel.mostrarMoradorDoImovel();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Metódo para excluir morador, onde eu passo o endereço/ cpf do morador que quuero excluir.
    public void excluirMoradorDoImovel(){

        String cpf;
        String endereco;


        IO.mostrarComentario("Digite o cpf do morador que deseja excluir: ");
        cpf = IO.usuarioDigita().nextLine();

        IO.mostrarComentario("Digite o endereço do morador que deseja excluir: ");
        endereco = IO.usuarioDigita().nextLine();

        Imovel enderecoImovel = null;

        try {
            enderecoImovel = imobiliaria.encontrarImovel(endereco);
            enderecoImovel.excluirMorador(cpf);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Metódo que exucuta as ações dos outros metódos exemplo cadastroUmMorador.
    public void executarSistema() throws Exception{

        menu();
        boolean executar = true;
        while (executar){

            String opcao = IO.usuarioDigita().nextLine();

            switch (opcao){
                case "1":
                    cadastrarImovel();
                    menu();
                    break;


                case "2":
                    mostraImovel();
                    menu();
                    break;

                case "3":
                    Funcionario.faleComUmVendedor();
                    menu();
                    break;

                case "4":
                    cadastroDeMorador();
                    menu();
                    break;

                case "5":
                    mostrarMorador();
                    menu();
                    break;

                case "7":
                    excluirMoradorDoImovel();
                    menu();
                    break;

                case "6":
                    executar = false;
                    break;

            }

        }

    }

}
